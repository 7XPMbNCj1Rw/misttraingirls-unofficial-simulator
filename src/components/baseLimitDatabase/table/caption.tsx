import React from "react";
import Table from "@material-ui/core/Table";
import TableContainer from "@material-ui/core/TableContainer";
import { useBaseLimitPageStyles } from "components/baseLimitDatabase/style";
import EditIcon from "@material-ui/icons/EditOutlined";

export const CaptionHeader: React.FC = () => {
  const classes = useBaseLimitPageStyles();
  return (
    <TableContainer>
      <Table className={classes.table} size="small">
        <caption>
          ベースキャラクター名でフィルタリングおよびソートが，各能力値でソートが可能です．
          <br />
          フィルタリング時は，選択されたベースにおける全実装レイヤーを育成した際の総合限界も表示されます．
        </caption>
      </Table>
    </TableContainer>
  );
};

export const CaptionFooter: React.FC = () => {
  const classes = useBaseLimitPageStyles();
  return (
    <TableContainer>
      <Table className={classes.table} size="small">
        <caption>
          表の値は直接編集可能です．
          <EditIcon style={{ fontSize: 14, paddingTop: 4 }} />
          で該当行が編集モードになります．
          <br />
          こちらの編集内容もクラウド上に自動で反映されます．
          <br />
          <br />
          厳密な調査により成長限界の確定値が判明している場合，情報提供フォームよりもこちらを直接編集したほうがスムーズです．
          <br />
          また，誤入力等で表の内容が一時的に汚染された場合にも簡単に修正措置を講じることが可能です．
        </caption>
      </Table>
    </TableContainer>
  );
};
