import React, { Component } from "react";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

import Title from "components/Title";
import { database } from "components/Firebase";
import { baseLimitPageStyles } from "components/baseLimitDatabase/style";
import { isInteger, initializeflatObjectDict, varMapsMTG, hpTobaseValue, typeTree } from "components/utils";
import varsMTG from "components/vars";

import InputFormRow, { uncoveredBasesType } from "components/baseLimitDatabase/submit/inputFormRow";
import LimitFormRow, { rowType, createRowData } from "components/baseLimitDatabase/submit/limitFormRow";
import CaptionFooter from "components/baseLimitDatabase/submit/caption";

type updatesType = { [K in typeTree["statusVar"]]?: { value: number; isMoreThanLimit: boolean } };
type updatesMetaType = { hp: number; baseValue: number };
type formattedInputsType = Record<string, string>;

interface BaseLimitSubmitProps extends WithStyles<typeof baseLimitPageStyles> {
  loadDatabase: () => void;
  uncoveredBases: uncoveredBasesType;
}

interface BaseLimitSubmitState {
  row: rowType;
  previous: rowType;
  isMoreThanLimit: Record<typeTree["statusVar"], boolean>;
  isValid: Record<string, boolean>;
  isValidNumber: Record<typeTree["statusVar"], boolean>;
  includeInvalidInput: boolean;
}

class BaseLimitSubmit extends Component<BaseLimitSubmitProps, BaseLimitSubmitState> {
  constructor(props) {
    super(props);
    const initialRow = createRowData(
      initializeflatObjectDict([...varsMTG.layerSpecifierVars, "hp", ...varsMTG.statusVars], "") as Record<
        keyof rowType,
        string
      >
    );
    this.state = {
      row: initialRow,
      previous: initialRow,
      isMoreThanLimit: initializeflatObjectDict([...varsMTG.statusVars], true) as Record<
        typeTree["statusVar"],
        boolean
      >,
      isValid: {
        ...initializeflatObjectDict([...varsMTG.layerSpecifierVars], false),
        hp: true,
        ...initializeflatObjectDict([...varsMTG.statusVars], true),
      },
      isValidNumber: initializeflatObjectDict([...varsMTG.statusVars], false) as Record<typeTree["statusVar"], boolean>,
      includeInvalidInput: true,
    };
  }

  // componentDidMount = () => {};

  // componentWillUnmount = () => {};

  updateDatabase = async (
    baseName: string,
    rarity: string,
    layerName: string,
    updatesMeta: updatesMetaType,
    updates: updatesType
  ) => {
    console.log(baseName);
    console.log(layerName);
    console.log(updatesMeta);
    console.log(updates);
    const { baseValue } = updatesMeta;
    const { loadDatabase } = this.props;
    loadDatabase();
    const { uncoveredBases } = this.props;
    const baseLimit = uncoveredBases[baseName][rarity][layerName];
    const updateFinalizer = {};

    // [NOTE] Object.entries(updates).forEach(([key, { value, isMoreThanLimit }]) => {
    // だとupdatesの各keyを ? にしているせいでundefinedの可能性を排除できず{ value, isMoreThanLimit }に例外発生
    // しかし一度keyだけ取り出せばそれはundefined keyではないという判定がされるらしくその後のupdates[key]の取り出しが可能な模様
    Object.keys(updates).forEach((key) => {
      const { value, isMoreThanLimit } = updates[key];
      const keyName = varMapsMTG.status.get(key as typeTree["statusVar"]) as typeTree["statusName"];
      const hierarchicalKeyName = [baseName, layerName, "baseLimit", keyName].join(".");
      const diff = value - baseValue;
      const currentMax = baseLimit[keyName].max;
      const currentMin = baseLimit[keyName].min;

      if (isMoreThanLimit) {
        if (currentMax == null || currentMax > diff) {
          updateFinalizer[[hierarchicalKeyName, "max"].join(".")] = diff;
          if (currentMin === diff) {
            updateFinalizer[[hierarchicalKeyName, "determined"].join(".")] = diff;
          }
        }
      } else if (currentMin == null || currentMin < diff + 10) {
        updateFinalizer[[hierarchicalKeyName, "min"].join(".")] = diff + 10;
        if (currentMax === diff + 10) {
          updateFinalizer[[hierarchicalKeyName, "determined"].join(".")].determined = diff + 10;
        }
      }
    });

    await database.doc("layers").update(updateFinalizer);
    console.log(updateFinalizer);
    loadDatabase(); // ここもローカルでのupdate処理に留めて読み込みを減らしたい そして上のawaitを消す
  };

  onToggleEditMode = () => {
    const { row, previous, isMoreThanLimit, includeInvalidInput } = this.state;

    if (row.isEditMode) {
      this.closeEditMode(row, previous, isMoreThanLimit, includeInvalidInput);
    } else {
      this.openEditMode(row);
    }
  };

  openEditMode = (row: rowType): void => {
    this.setState({ previous: row, row: { ...row, isEditMode: true } });
  };

  closeEditMode = (
    row: rowType,
    previous: rowType,
    isMoreThanLimit: Record<typeTree["statusVar"], boolean>,
    includeInvalidInput: boolean
  ): void => {
    const { hp } = row;

    if (isInteger(hp)) {
      const hpNum = parseInt(hp, 10);
      const { baseName } = row;
      const { rarity, layerName } = row;
      const [updates, updatesMeta, formattedInputs] = this.handleEditedValidChanges(
        baseName,
        rarity,
        layerName,
        hpNum,
        row,
        previous,
        isMoreThanLimit
      );

      if (baseName !== "" && layerName !== "" && Object.keys(updates).length) {
        this.updateDatabase(baseName, rarity, layerName, updatesMeta, updates);
      }
      this.setState({ row: { ...previous, ...formattedInputs, isEditMode: includeInvalidInput } });
    }
  };

  handleEditedValidChanges = (
    baseName: string,
    rarity: string,
    layerName: string,
    hp: number,
    row: rowType,
    previous: rowType,
    isMoreThanLimit: Record<typeTree["statusVar"], boolean>
  ): [updatesType, updatesMetaType, formattedInputsType] => {
    const baseValue = hpTobaseValue(hp);
    const updates: updatesType = {};
    const updatesMeta: updatesMetaType = { hp, baseValue };
    const formattedInputs: formattedInputsType = { baseName, rarity, layerName, hp: hp.toString() };

    for (const key of varsMTG.statusVars) {
      const valueStr = row[key];
      const previousValue = previous[key];
      if (isInteger(valueStr)) {
        const value = parseInt(valueStr, 10);
        if (value !== parseInt(previousValue, 10)) {
          formattedInputs[key] = value.toString();
        }
        updates[key] = { value, isMoreThanLimit: isMoreThanLimit[key] };
      } else if (valueStr === "") {
        formattedInputs[key] = "";
      }
    }

    return [updates, updatesMeta, formattedInputs];
  };

  onChange = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, row: rowType, name: string): void => {
    const { value } = e.target;
    let { isValid, isValidNumber } = this.state;

    if (varMapsMTG.layerSpecifier.has(name as typeTree["layerSpecifierVar"])) {
      isValid = this.handleValidLayerSpecification(row, name, value, isValid);
    } else {
      [isValid, isValidNumber] = this.handleValidInput(row, name, value, isValid, isValidNumber);
    }
    this.setState({
      isValid,
      isValidNumber,
      includeInvalidInput: !(Object.values(isValid).every((v) => v) && Object.values(isValidNumber).some((v) => v)),
    });
  };

  handleValidLayerSpecification = (row, name, value, isValid) => {
    const tmpRow = { ...row, [name]: value };
    const { baseName } = tmpRow;
    let { rarity, layerName } = tmpRow;
    const { uncoveredBases } = this.props;
    let newIsValid = { ...isValid, [name]: true };

    if (!(rarity in uncoveredBases[baseName])) {
      rarity = "";
      layerName = "";
      newIsValid = { ...newIsValid, rarity: false, layerName: false };
    } else if (!(layerName in uncoveredBases[baseName][rarity])) {
      layerName = "";
      newIsValid = { ...newIsValid, layerName: false };
    }
    this.setState({ row: { ...row, baseName, rarity, layerName } });

    return newIsValid;
  };

  handleValidInput = (row, name, value, isValid, isValidNumber) => {
    const numberFlag = isInteger(value);
    this.setState({ row: { ...row, [name]: value } });
    if (name === "hp") {
      return [{ ...isValid, hp: numberFlag }, isValidNumber];
    }
    return [
      { ...isValid, [name]: numberFlag || value === "" },
      { ...isValidNumber, [name]: numberFlag },
    ];
  };

  onRevert = () => {
    const { previous } = this.state;
    this.setState({ row: { ...previous, isEditMode: false } });
  };

  onReset = () => {
    this.setState({
      row: createRowData(
        initializeflatObjectDict([...varsMTG.layerSpecifierVars, "hp", ...varsMTG.statusVars], "") as Record<
          keyof rowType,
          string
        >
      ),
      isMoreThanLimit: initializeflatObjectDict([...varsMTG.statusVars], true) as Record<
        typeTree["statusVar"],
        boolean
      >,
      isValid: {
        ...initializeflatObjectDict([...varsMTG.layerSpecifierVars], false),
        hp: true,
        ...initializeflatObjectDict([...varsMTG.statusVars], true),
      },
      isValidNumber: initializeflatObjectDict([...varsMTG.statusVars], false) as Record<typeTree["statusVar"], boolean>,
      includeInvalidInput: true,
    });
  };

  handleSwitchLimitIcon = (key: typeTree["statusVar"]): void => {
    const { isMoreThanLimit } = this.state;
    this.setState({
      isMoreThanLimit: {
        ...isMoreThanLimit,
        [key]: !isMoreThanLimit[key],
      },
    });
  };

  render() {
    const { onChange, onRevert, onReset, onToggleEditMode, handleSwitchLimitIcon } = this;
    const { row, isMoreThanLimit, includeInvalidInput } = this.state;
    const { classes, uncoveredBases } = this.props;

    return (
      <>
        <Title>情報提供フォーム</Title>
        <Table className={classes.table} aria-label="caption table" size="small">
          <TableHead>
            <TableRow>
              <TableCell colSpan={4} />
              <TableCell align="center" size="small" className={classes.tableHeader}>
                HP
              </TableCell>
              {[...varMapsMTG.status].map(([key, name]) => (
                <TableCell key={`${key}.header`} align="center" size="small" className={classes.tableHeader}>
                  {name}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            <InputFormRow
              {...{
                row,
                onToggleEditMode,
                onChange,
                uncoveredBases,
                includeInvalidInput,
              }}
            />
            <LimitFormRow
              {...{
                row,
                onRevert,
                onReset,
                handleSwitchLimitIcon,
                isMoreThanLimit,
              }}
            />
          </TableBody>
        </Table>
        <CaptionFooter />
      </>
    );
  }
}
export default withStyles(baseLimitPageStyles, { withTheme: true })(BaseLimitSubmit);
