import React from "react";
import Typography from "@material-ui/core/Typography";

interface TitleProps {
  children?: React.ReactNode;
}

const Title: React.FC<TitleProps> = (props) => {
  const { children } = props;
  return (
    <Typography component="h3" variant="subtitle1" color="primary" gutterBottom>
      {children}
    </Typography>
  );
};
export default Title;
