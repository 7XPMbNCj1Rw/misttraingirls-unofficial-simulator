import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

// Github Pagesで公開する関係上これらは平で書かれているが，せいぜいAPIを用いてデータベースの読み書きができる程度で
// セキュリティ上の実害はないため問題ない（これだけではコンソールにはアクセスできないため課金関係での悪用可能性もなし）
// 参考：https://qiita.com/hoshymo/items/e9c14ed157200b36eaa5
//      https://stackoverflow.com/questions/37482366/is-it-safe-to-expose-firebase-apikey-to-the-public
// 本プロジェクトでfirestoreに格納されるデータは検証されたゲーム内数値のみであるため，個人情報漏洩の恐れもない
const firebaseConfig = {
  apiKey: "AIzaSyB1O0MzwVtNhcSvUYapcAWZGWW6pzRwctU",
  authDomain: "misttraingirls-unofficial.firebaseapp.com",
  projectId: "misttraingirls-unofficial",
  storageBucket: "misttraingirls-unofficial.appspot.com",
  messagingSenderId: "616796851889",
  appId: "1:616796851889:web:38027b60727a2e802d5c90",
  measurementId: "G-6YV5QVPQDW",
};

firebase.initializeApp(firebaseConfig);

export const db = firebase.firestore();
export const database = db.collection("database");
export default firebase;
