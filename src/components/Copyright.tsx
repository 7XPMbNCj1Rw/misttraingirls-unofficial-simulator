import React from "react";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";

export default function Copyright(): JSX.Element {
  return (
    <div>
      <Typography variant="body2" color="textSecondary" align="center">
        {"Related Link: "}
        <Link color="inherit" href="https://misttraingirls.wikiru.jp/">
          ミストトレインガールズ攻略 Wiki
        </Link>
        {new Date().getFullYear()}
        {/* <br />
        <Link color="inherit" href="https://gitlab.com/misttraingirls-unofficial/misttraingirls-unofficial-simulator">
          GitLabプロジェクトページ
        </Link> */}
      </Typography>
    </div>
  );
}
