import { StyleRules } from "@material-ui/core/styles/withStyles";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

export const baseLimitPageStyles = (theme: Theme): StyleRules =>
  createStyles({
    paper: {
      padding: theme.spacing(2),
      display: "flex",
      overflow: "auto",
      flexDirection: "column",
    },
    baseLimitTable: {
      // height: "45vh",
    },
    baseLimitSubmit: {
      // maxHeight: "45vh",
    },
    table: {
      minWidth: 1200,
    },
    tableContainer: {
      height: "35vh",
    },
    basenameSorter: {
      fontSize: 11,
      paddingLeft: theme.spacing(8),
    },
    inputFilter: {
      height: 15,
      flex: 1,
      fontSize: 11,
    },
    editRowCell: {
      fontSize: 11,
    },
    selectTableCell: {
      width: 30,
      fontSize: 11,
    },
    tableCell: {
      height: 34,
      fontSize: 11,
    },
    tableHeader: {
      fontSize: 11,
    },
    input: {
      height: 15,
      flex: 1,
      fontSize: 11,
    },
    inputSelect: {
      height: 15,
      flex: 1,
      fontSize: 11,
      margin: -theme.spacing(1),
      // padding: -10
    },
    baseNameSelect: {
      height: 15,
      flex: 1,
      fontSize: 11,
      marginTop: -theme.spacing(0.1),
    },
    limitIcon: {
      // paddingRight: 0
    },
    dropdownStyle: {
      maxHeight: 300,
    },
  });

// [NOTE] 期待としては const useBaseLimitPageStyles = makeStyles(baseLimitPageStyles);
// で同内容のはずなのだが、そちらだとstylesが全く適用されないことを確認済
// 各関数コンポーネント内で呼び出される際に取得される情報に差異があるのかもしれない
export function useBaseLimitPageStyles(): Record<string, string> {
  return makeStyles(baseLimitPageStyles)();
}

export const menuProps = (
  classes: Record<string, string>
): Record<string, Record<string, string> | StyleRules | null> => ({
  anchorOrigin: {
    vertical: "bottom",
    horizontal: "left",
  },
  transformOrigin: {
    vertical: "top",
    horizontal: "left",
  },
  getContentAnchorEl: null,
  classes: {
    paper: classes.dropdownStyle,
  },
});
