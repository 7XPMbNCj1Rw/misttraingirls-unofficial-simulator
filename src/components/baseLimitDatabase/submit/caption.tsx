import React from "react";
import Table from "@material-ui/core/Table";
import TableContainer from "@material-ui/core/TableContainer";

import EditIcon from "@material-ui/icons/EditOutlined";
import Star from "@material-ui/icons/Star";
import KeyboardCapslockIcon from "@material-ui/icons/KeyboardCapslock";
import RotateLeftIcon from "@material-ui/icons/RotateLeft";
import { useBaseLimitPageStyles } from "components/baseLimitDatabase/style";

const CaptionFooter: React.FC = () => {
  const classes = useBaseLimitPageStyles();
  return (
    <TableContainer>
      <Table className={classes.table} size="small">
        <caption>
          情報提供の際は，上記より現在のHPと各種ベース能力値を入力してください．
          <br />
          また，成長MAX表示
          <Star style={{ fontSize: 14, paddingTop: 4 }} />
          と成長可能表示
          <KeyboardCapslockIcon style={{ fontSize: 14, paddingTop: 4 }} />
          を適宜切り替えてください．
          <br />
          <br />
          ご提供いただく数値が厳密な成長限界である必要はありません．また空欄があっても問題ありません．
          <br />
          <EditIcon style={{ fontSize: 14, paddingTop: 4 }} />
          で入力モードになり，
          <RotateLeftIcon style={{ fontSize: 14, paddingTop: 4 }} />
          でフォームがリセットされます．
          <br />
          <br />
          提供された数値はクラウド上に自動で集計・反映され，表は自動で更新されます．
          <br />
          集計された情報に基づき各値のとりうる範囲が絞られていきます．十分な情報が出揃えば値が確定するという仕組みです．
        </caption>
      </Table>
    </TableContainer>
  );
};

export default CaptionFooter;
