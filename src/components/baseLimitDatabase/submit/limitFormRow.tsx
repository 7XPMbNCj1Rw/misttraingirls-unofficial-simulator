import React from "react";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@material-ui/core/IconButton";

import RevertIcon from "@material-ui/icons/NotInterestedOutlined";
import Star from "@material-ui/icons/Star";
import KeyboardCapslockIcon from "@material-ui/icons/KeyboardCapslock";
import RotateLeftIcon from "@material-ui/icons/RotateLeft";
import { red } from "@material-ui/core/colors";

import { useBaseLimitPageStyles } from "components/baseLimitDatabase/style";
import varsMTG from "components/vars";
import { typeTree } from "components/utils";

export type rowType = {
  baseName: string;
  rarity: string;
  layerName: string;
  hp: string;
  phyAtk: string;
  phyDef: string;
  hit: string;
  speed: string;
  magAtk: string;
  magdef: string;
  heal: string;
  luck: string;
  isEditMode: boolean;
};

export function createRowData(props: {
  baseName: string;
  rarity: string;
  layerName: string;
  hp: string;
  phyAtk: string;
  phyDef: string;
  hit: string;
  speed: string;
  magAtk: string;
  magdef: string;
  heal: string;
  luck: string;
}): rowType {
  return {
    ...props,
    isEditMode: false,
  };
}

type SwitchableLimitIconButtonType = {
  isMoreThanLimit: boolean;
  onClick: () => void;
};

const SwitchableLimitIconButton: React.FC<SwitchableLimitIconButtonType> = (props) => {
  const classes = useBaseLimitPageStyles();
  const { isMoreThanLimit, onClick } = props;
  return (
    <IconButton size="small" onClick={onClick} className={classes.limitIcon}>
      {isMoreThanLimit ? <Star style={{ fontSize: 14 }} /> : <KeyboardCapslockIcon style={{ fontSize: 14 }} />}
    </IconButton>
  );
};

type LimitFormRowType = {
  row: rowType;
  onRevert: () => void;
  onReset: () => void;
  handleSwitchLimitIcon: (key: typeTree["statusVar"]) => void;
  isMoreThanLimit: Record<typeTree["statusVar"], boolean>;
};

const LimitFormRow: React.FC<LimitFormRowType> = (props) => {
  const classes = useBaseLimitPageStyles();
  const { row, onRevert, onReset, handleSwitchLimitIcon, isMoreThanLimit } = props;
  return (
    <TableRow>
      <TableCell className={classes.selectTableCell} size="small">
        {row.isEditMode ? (
          <IconButton aria-label="revert" size="small" onClick={onRevert} style={{ color: red[500] }}>
            <RevertIcon style={{ fontSize: 14 }} />
          </IconButton>
        ) : (
          <IconButton aria-label="reset" size="small" onClick={onReset}>
            <RotateLeftIcon style={{ fontSize: 14 }} />
          </IconButton>
        )}
      </TableCell>
      <TableCell className={classes.tableCell} style={{ width: 160 + 80 + 220 + 70 }} colSpan={4} />
      {varsMTG.statusVars.map((key) => (
        <TableCell
          key={`limitFlag.${key}`}
          className={classes.tableCell}
          align="center"
          style={{ width: 70 }}
          size="small"
        >
          <SwitchableLimitIconButton
            isMoreThanLimit={isMoreThanLimit[key]}
            onClick={() => handleSwitchLimitIcon(key)}
          />
        </TableCell>
      ))}
    </TableRow>
  );
};
export default LimitFormRow;
