import React, { Component } from "react";
import clsx from "clsx";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { FilterOptionsState } from "@material-ui/lab";
import firebase from "firebase/app";

import { database } from "components/Firebase";
import { baseLimitPageStyles } from "components/baseLimitDatabase/style";
import {
  isInteger,
  layerDataType,
  varMapsMTG,
  normalizeBaseName,
  initializeflatObjectDict,
  typeTree,
} from "components/utils";
import varsMTG from "components/vars";
import {
  rowType,
  createRowData,
  limitsInputType,
  limitStagesType,
} from "components/baseLimitDatabase/table/customTableRow";
import BaseLimitSubmit from "./submit/baseLimitSubmit";
import BaseLimitTable from "./table/baseLimitTable";
import { uncoveredBasesType } from "./submit/inputFormRow";

type updatesType = { [K in typeTree["statusVar"]]?: number | null };
type formattedInputsType = { [K in typeTree["statusVar"]]?: string };
type BaseLimitPageProps = WithStyles<typeof baseLimitPageStyles>;

type BaseLimitPageState = {
  baseNames: { name: string; key: string }[];
  uncoveredBases: uncoveredBasesType;
  originalRows: rowType[];
  rows: rowType[];
  previous: Record<string, rowType>;
  sortOrder: "asc" | "desc";
  sortKey: string;
  filterBase: { name: string; key: string } | null;
};

class BaseLimitPage extends Component<BaseLimitPageProps, BaseLimitPageState> {
  private filterAll = "すべて";

  constructor(props) {
    super(props);
    const initialRow = createRowData({
      baseName: "",
      rarity: "",
      idx: 0,
      layerName: "",
      limits: initializeflatObjectDict([...varsMTG.statusVars], "") as limitsInputType,
      limitStages: initializeflatObjectDict([...varsMTG.statusVars], 0) as limitStagesType,
      isEditable: false,
    });
    this.state = {
      baseNames: [{ name: this.filterAll, key: this.filterAll }],
      uncoveredBases: {},
      originalRows: [initialRow],
      rows: [initialRow],
      previous: {},
      sortOrder: "asc",
      sortKey: "baseName",
      filterBase: null,
    };
    this.loadDatabase();
  }

  // componentDidMount = () => {};

  // componentWillUnmount() {}

  loadDatabase = (): void => {
    console.log("start fetching database");
    database
      .doc("layers")
      .get()
      .then((doc) => {
        console.log(`retained data: ${doc.id}`);
        this.parseLayerDatabase(doc.data());
        console.log("finish post processing");
      })
      .catch((err) => {
        console.log("Error getting documents", err);
      });
  };

  parseLayerDatabase = (data: firebase.firestore.DocumentData | undefined): void => {
    const { filterBase, sortOrder, sortKey } = this.state;
    const rows: rowType[] = [];
    const baseNames: string[] = [];
    const uncoveredBases: BaseLimitPageState["uncoveredBases"] = {};

    if (typeof data != "undefined") {
      for (const [baseName, baseData] of Object.entries(data) as [string, { [key: string]: layerDataType }][]) {
        if (baseName !== "templateCharactor") {
          baseNames.push(baseName);
          const uncoveredBase = {};
          for (const [layerName, layerData] of Object.entries(baseData) as [string, layerDataType][]) {
            if (layerName !== "rarityCounter") {
              const { baseLimit, rarity, idx } = layerData;
              // BaseLimitTableのための処理
              const [limits, limitStages] = this.extractBaseLimitInfo(baseLimit);
              rows.push(
                createRowData({
                  baseName,
                  rarity,
                  idx,
                  layerName,
                  limits,
                  limitStages,
                })
              );

              // BaseLimitSubmitのための処理
              if (Object.values(limits).some((value) => !Number.isInteger(value))) {
                if (!(rarity in uncoveredBase)) {
                  uncoveredBase[rarity] = {};
                }
                uncoveredBase[rarity][layerName] = baseLimit;
              }
            }
          }
          if (Object.keys(uncoveredBase).length > 0) {
            uncoveredBases[baseName] = uncoveredBase;
          }
        }
      }
    }

    rows.sort((a, b) => {
      for (const key of ["baseName", "rarity", "idx", "id"]) {
        if (b[key] > a[key]) {
          return -1;
        }
        if (b[key] < a[key]) {
          return 1;
        }
      }
      return 0;
    });

    this.setState({
      baseNames: [{ name: this.filterAll, key: this.filterAll }].concat(
        baseNames.sort().map((baseName) => ({ name: baseName, key: normalizeBaseName(baseName) }))
      ),
      uncoveredBases,
      originalRows: rows,
    });

    this.dataFiltering(filterBase);
    this.sortColumn(sortKey, sortOrder);
  };

  extractBaseLimitInfo = (data: layerDataType["baseLimit"]): [limitsInputType, limitStagesType] => {
    const limits = {} as limitsInputType;
    const limitStages = {} as limitStagesType;
    for (const [key, name] of varMapsMTG.status.entries()) {
      const statusData = data[name];
      const { min, max } = statusData;
      const { determined, limitStage } = statusData;
      const minStr = min == null ? "?" : min;
      const maxStr = max == null ? "?" : max;
      if (determined == null) {
        limits[key] = minStr === "?" && maxStr === "?" ? "?" : `[${minStr} ~ ${maxStr}]`;
      } else {
        limits[key] = determined.toString();
      }
      limitStages[key] = limitStage;
    }

    return [limits, limitStages];
  };

  updateDatabase = async (updates: updatesType): Promise<void> => {
    if (Object.keys(updates).length) {
      console.log(`database update query:`);
      console.log(updates);
      await database.doc("layers").update(updates);
      this.loadDatabase(); // TODO: localのstateを変更するなどに書き換えて読み込みを減らす そして上のawaitを消す
    }
  };

  onToggleEditMode = (id: string): void => {
    const { rows, previous } = this.state;
    this.setState({
      rows: rows.map((row) => {
        if (row.id === id) {
          if (row.isEditMode) {
            return this.closeEditMode(id, row, previous);
          }
          return this.openEditMode(row, previous);
        }
        return row;
      }),
    });
  };

  openEditMode = (row: rowType, previous: Record<string, rowType>): rowType => {
    this.setState({ previous: { ...previous, [row.id]: row } });
    return { ...row, isEditMode: true };
  };

  closeEditMode = (id: string, row: rowType, previous: Record<string, rowType>): rowType => {
    const previousRow = previous[id];
    const [updates, formattedInputs] = this.handleEditedValidChanges(row, previousRow);

    this.updateDatabase(updates);
    return { ...previousRow, ...formattedInputs, isEditMode: false };
  };

  handleEditedValidChanges = (row: rowType, previousRow: rowType): [updatesType, formattedInputsType] => {
    const { baseName, layerName } = row;
    const updates: updatesType = {};
    const formattedInputs: formattedInputsType = {};

    for (const [key, name] of varMapsMTG.status.entries()) {
      const value = row[key];
      const previousValue = previousRow[key];
      if (value !== previousValue) {
        const updateKey = [baseName, layerName, "baseLimit", name, "determined"].join(".");
        if (value === "?") {
          formattedInputs[key] = "?";
          updates[updateKey] = null;
        } else if (isInteger(value)) {
          const valueNum = parseInt(value, 10);
          if (valueNum !== parseInt(previousValue, 10)) {
            formattedInputs[key] = valueNum.toString();
            updates[updateKey] = valueNum;
          }
        }
      }
    }

    return [updates, formattedInputs];
  };

  onRevert = (id: string): void => {
    const { rows, previous } = this.state;

    this.setState({
      rows: rows.map((row) => {
        if (row.id === id) {
          return previous[id];
        }
        return row;
      }),
    });
  };

  onInputChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, id: string): void => {
    const { rows } = this.state;
    const { name, value } = e.target;

    const newRows = rows.map((row) => {
      if (row.id === id) {
        return { ...row, [name]: value };
      }
      return row;
    });

    this.setState({ rows: newRows });
  };

  onChangefilter = (e: React.ChangeEvent<unknown>, filterBase: { name: string; key: string } | null): void => {
    this.setState({ filterBase });
    this.dataFiltering(filterBase);
  };

  dataFiltering = (filterBase: { name: string; key: string } | null): void => {
    const { baseNames, originalRows } = this.state;

    if (!filterBase || filterBase.key === this.filterAll || filterBase.key === "") {
      this.setState({ rows: originalRows });
    } else if (baseNames.some((b) => b.key === filterBase.key)) {
      const filteredRows = originalRows.filter((row) => row.baseName === filterBase.name);
      this.setState({
        rows: [this.computeTotalBaseLimit(filterBase, filteredRows)].concat(filteredRows),
      });
    }
  };

  computeTotalBaseLimit = (filterBase: { name: string; key: string }, filteredRows: rowType[]): rowType => {
    const limits = {} as limitsInputType;
    const limitStages = {} as limitStagesType;

    for (const key of varsMTG.statusVars) {
      let tmpMax = -1000;
      filteredRows.forEach((row) => {
        const valueStr = row[key];
        const value = parseInt(valueStr, 10);
        if (value > tmpMax) {
          tmpMax = value;
          limits[key] = valueStr;
          limitStages[key] = row.limitStages[key];
        }
      });
    }

    return createRowData({
      baseName: filterBase.name,
      rarity: "",
      idx: 0,
      layerName: "(全レイヤーで育成)",
      limits,
      limitStages,
      isEditable: false,
    });
  };

  handleClickSortColumn = (key: string) => (): void => {
    const { sortKey, sortOrder } = this.state;
    let nextOrder: "asc" | "desc";
    if (key === sortKey) {
      nextOrder = sortOrder === "desc" ? "asc" : "desc";
    } else {
      nextOrder = key === "baseName" ? "asc" : "desc";
    }
    this.sortColumn(key, nextOrder);
    this.setState({
      sortOrder: nextOrder,
      sortKey: key,
    });
  };

  sortColumn = (key: string, order: "asc" | "desc"): void => {
    const { rows } = this.state;
    // ベース名によるソート順だけ特殊なので処理を施す
    const isBaseName = key === "baseName";
    const sortRule = { asc: [1, -1], desc: [-1, 1] }[order];

    const sortedRows = rows.slice().sort((a, b) => {
      const [aKey, bKey] = [a[key], b[key]];
      if (isBaseName) {
        if (aKey > bKey) {
          return sortRule[0];
        }
        if (aKey < bKey) {
          return sortRule[1];
        }
      } else {
        if (isInteger(aKey)) {
          if (isInteger(bKey)) {
            if (aKey > bKey) {
              return sortRule[0];
            }
            if (aKey < bKey) {
              return sortRule[1];
            }
          } else {
            return -1;
          }
        } else if (isInteger(bKey)) {
          return 1;
        }
        if (a.baseName > b.baseName) {
          return 1;
        }
        if (a.baseName < b.baseName) {
          return -1;
        }
      }
      if (a.rarity > b.rarity) {
        return 1;
      }
      if (a.rarity < b.rarity) {
        return -1;
      }
      if (a.idx > b.idx) {
        return 1;
      }
      if (a.idx < b.idx) {
        return -1;
      }
      return 0;
    });

    this.setState({ rows: sortedRows });
  };

  baseNameFilterOptions = (
    options: { name: string; key: string }[],
    { inputValue }: FilterOptionsState<{ name: string; key: string }>
  ): { key: string; name: string }[] => {
    if (!inputValue) {
      return options;
    }
    const searchBaseNameKey = normalizeBaseName(inputValue);
    return options.filter((option) => option.key.indexOf(searchBaseNameKey) === 0);
  };

  render() {
    const {
      loadDatabase,
      filterAll,
      onInputChange,
      onToggleEditMode,
      onRevert,
      onChangefilter,
      baseNameFilterOptions,
      handleClickSortColumn,
    } = this;
    const { baseNames, uncoveredBases, originalRows, rows, previous, sortOrder, sortKey, filterBase } = this.state;
    const { classes } = this.props;

    return (
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className={clsx(classes.paper, classes.baseLimitSubmit)}>
            <BaseLimitSubmit {...{ loadDatabase, uncoveredBases }} />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper className={clsx(classes.paper, classes.baseLimitTable)}>
            <BaseLimitTable
              {...{
                loadDatabase,
                baseNames,
                filterAll,
                originalRows,
                rows,
                previous,
                sortOrder,
                sortKey,
                filterBase,
                onInputChange,
                onToggleEditMode,
                onRevert,
                onChangefilter,
                baseNameFilterOptions,
                handleClickSortColumn,
              }}
            />
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(baseLimitPageStyles, { withTheme: true })(BaseLimitPage);
