import React from "react";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Input from "@material-ui/core/Input";
import IconButton from "@material-ui/core/IconButton";

import EditIcon from "@material-ui/icons/EditOutlined";
import DoneIcon from "@material-ui/icons/DoneAllTwoTone";
import LockIcon from "@material-ui/icons/Lock";
import RevertIcon from "@material-ui/icons/NotInterestedOutlined";
import { green, red } from "@material-ui/core/colors";

import { useBaseLimitPageStyles } from "components/baseLimitDatabase/style";
import { typeTree } from "components/utils";
import varsMTG from "components/vars";

export type limitsInputType = { [K in typeTree["statusVar"]]: string };
export type limitStagesType = { [K in typeTree["statusVar"]]: 0 | 1 | 2 };

export type rowType = {
  id: string;
  idx: number;
  baseName: string;
  rarity: string;
  layerName: string;
  // ここも本当は[K in typeTree["statusVar"]]で展開したいが他の要素と同じ階層になると怒られる
  phyAtk: string;
  phyDef: string;
  hit: string;
  speed: string;
  magAtk: string;
  magdef: string;
  heal: string;
  luck: string;
  limitStages: limitStagesType;
  isEditMode: boolean;
  isEditable: boolean;
  isLocked: boolean;
};

export const createRowData = ({
  baseName,
  rarity,
  idx,
  layerName,
  limits,
  limitStages,
  isEditable = true,
}: {
  baseName: string;
  rarity: string;
  idx: number;
  layerName: string;
  limits: limitsInputType;
  limitStages: limitStagesType;
  isEditable?: boolean;
}): rowType =>
  // let isLocked = true;
  // for (const key of status.keys()) {
  //   if (!isInteger(limits[key])) {
  //     isLocked = false;
  //     break;
  //   }
  // }
  ({
    id: [baseName, rarity, idx, layerName].join("."),
    idx,
    baseName,
    rarity,
    layerName,
    ...limits,
    limitStages,
    isEditMode: false,
    isEditable,
    isLocked: false,
  });

type EditButtonType = {
  row: rowType;
  onToggleEditMode: (id: string) => void;
  onRevert: (id: string) => void;
};

const EditButton: React.FC<EditButtonType> = (props) => {
  const { row, onToggleEditMode, onRevert } = props;
  if (row.isLocked) {
    return <LockIcon style={{ fontSize: 14 }} />;
  }
  if (row.isEditMode) {
    return (
      <>
        <IconButton
          aria-label="done"
          size="small"
          onClick={() => onToggleEditMode(row.id)}
          style={{ color: green[500] }}
        >
          <DoneIcon style={{ fontSize: 14 }} />
        </IconButton>
        <IconButton
          aria-label="revert"
          size="small"
          onClick={() => onRevert(row.id)}
          style={{ color: red[500], marginLeft: 10 }}
        >
          <RevertIcon style={{ fontSize: 14 }} />
        </IconButton>
      </>
    );
  }
  return (
    <IconButton aria-label="edit" size="small" onClick={() => onToggleEditMode(row.id)}>
      <EditIcon style={{ fontSize: 14 }} />
    </IconButton>
  );
};

type CustomTableCellType = {
  row: rowType;
  name: keyof rowType;
  width?: number;
  align?: "left" | "center" | "right";
  onInputChange: (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, id: string) => void;
  disabled?: boolean;
  color?: string;
  fontWeight?: "normal" | "bold";
};

const CustomTableCell: React.FC<CustomTableCellType> = (props) => {
  const { row, name, width, align, onInputChange, disabled = false, color = "#000000", fontWeight = "normal" } = props;
  const classes = useBaseLimitPageStyles();
  const { isEditMode, id } = row;
  return !disabled && isEditMode ? (
    <TableCell align={align} className={classes.tableCell} style={{ width }} size="small">
      <Input
        value={row[name]}
        name={name}
        onChange={(e) => onInputChange(e, id)}
        autoComplete="off"
        fullWidth
        className={classes.input}
        margin="dense"
      />
    </TableCell>
  ) : (
    <TableCell align={align} className={classes.tableCell} style={{ width, color, fontWeight }} size="small">
      {row[name]}
    </TableCell>
  );
};

type CustomTableRowType = {
  row: rowType;
  onToggleEditMode: (id: string) => void;
  onRevert: (id: string) => void;
  onInputChange: (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, id: string) => void;
};

const CustomTableRow: React.FC<CustomTableRowType> = (props) => {
  const classes = useBaseLimitPageStyles();
  const { row, onToggleEditMode, onRevert, onInputChange } = props;
  const cellProps: { name: keyof rowType; width: number; align: "left" | "center" | "right" }[] = [
    { name: "baseName", width: 140, align: "left" },
    { name: "rarity", width: 60, align: "center" },
    { name: "layerName", width: 180, align: "left" },
  ];
  const limitStageFontProperties: { color: string; fontWeight: "normal" | "bold" }[] = [
    { color: "#000000", fontWeight: "normal" },
    { color: "#ff8c00", fontWeight: "bold" },
    { color: "#d2691e", fontWeight: "bold" },
  ];
  return (
    <TableRow>
      <TableCell className={classes.editRowCell} style={{ width: 30 }} size="small">
        {row.isEditable ? <EditButton {...{ row, onToggleEditMode, onRevert }} /> : <></>}
      </TableCell>
      {cellProps.map((cellProp) => (
        <CustomTableCell
          key={`form.${cellProp.name}`}
          {...{
            row,
            onInputChange,
            disabled: true,
            ...cellProp,
          }}
        />
      ))}
      {varsMTG.statusVars.map((key) => (
        <CustomTableCell
          key={`${row.id}.${key}`}
          {...{
            row,
            name: key,
            width: 75,
            align: "right",
            onInputChange,
            ...limitStageFontProperties[row.limitStages[key]],
          }}
        />
      ))}
    </TableRow>
  );
};

export default CustomTableRow;
