import varsMTG from "./vars";

// util functions
export function isInteger(value: string): boolean {
  return /^-{0,1}\d+$/.test(value);
}

export function zip<T, U>(a: readonly T[], b: readonly U[]): [T, U][] {
  return a.map((ai, i) => [ai, b[i]]);
}

export function normalizeBaseName(baseName: string): string {
  if (!baseName) {
    return baseName;
  }
  return baseName
    .trim()
    .replace(/[ぁ-ん]/g, (b) => String.fromCharCode(b.charCodeAt(0) + 0x60))
    .replace(/[Ａ-Ｚａ-ｚ０-９]/g, (b) => String.fromCharCode(b.charCodeAt(0) - 0xfee0));
}

export function initializeflatObjectDict<T>(keys: string[], value: T): { [key: string]: T } {
  const mapDict = new Map(keys.map((key) => [key, value]));
  return Object.fromEntries(mapDict);
}

export function initializePairedObjectDict<T>(keys: string[], values: T[]): { [key: string]: T } {
  const mapDict = new Map(keys.map((key, i) => [key, values[i]]));
  return Object.fromEntries(mapDict);
}

// データベースの共通フォーマット型指定

// 要素ごとのtypeのUnionを取る
// （ref: https://dev.to/shakyshane/2-ways-to-create-a-union-from-an-array-in-typescript-1kd6,
//       https://stackoverflow.com/questions/52085454/typescript-define-a-union-type-from-an-array-of-strings,
//       https://qiita.com/Quramy/items/e27a7756170d06bef22a,
//       https://zenn.dev/tak_iwamoto/articles/d367f989eb4a33）
export type layerSpecifierTagsUnion = typeof varsMTG.layerSpecifierTags[number];
export type statusNamesUnion = typeof varsMTG.statusNames[number];
export type layerTypesUnion = typeof varsMTG.layerTypes[number];
export type weaponTypesUnion = typeof varsMTG.weaponTypes[number];
export type elementsUnion = typeof varsMTG.elements[number];

export type layerSpecifierVarsUnion = typeof varsMTG.layerSpecifierVars[number];
export type statusVarsUnion = typeof varsMTG.statusVars[number];
export type elementVarsUnion = typeof varsMTG.elemenVars[number];

// .jsonに保存しているレイヤー情報の型
export type layerDataType = {
  rarity: string;
  idx: number;
  type: layerTypesUnion;
  weapon: weaponTypesUnion;
  resistance: {
    [K in elementsUnion]: number | null;
  };
  baseLimit: {
    [K in statusNamesUnion]: {
      min: number | null;
      max: number | null;
      determined: number | null; // 後々undefinedを処理可能にし、null（明示的にその値がないことを示す）と区別したい
      limitStage: 0 | 1 | 2;
    };
  };
  statusMagnification: {
    [key: string]: { [K in statusNamesUnion]: number | null };
  };
  LLvUpStep: { [K in statusNamesUnion]: number | null };
  GLvUpStep: { [K in statusNamesUnion]: number | null };
  skill: string[];
  ability: string[];
  specialSkill: string | null;
};

// importを簡略化・統一化できるようひとつのtypeにまとめる
// 運用時は Pick<typeTree, "**"> や typeTree["*"] のように任意のフィールドを取得
export type typeTree = {
  layerSpecifierTag: layerSpecifierTagsUnion;
  statusName: statusNamesUnion;
  layerType: layerTypesUnion;
  weaponType: weaponTypesUnion;
  element: elementsUnion;
  layerSpecifierVar: layerSpecifierVarsUnion;
  statusVar: statusVarsUnion;
  elementVar: elementVarsUnion;
  layerData: layerDataType;
};

// ミストレ固有名詞群と対応変数名の共通変換を定義
// 順序を保持するMapの特性がObjectよりも好ましいためこちらで辞書を定義している
export class varMapsMTG {
  static readonly layerSpecifier: Map<layerSpecifierVarsUnion, layerSpecifierTagsUnion> = new Map(
    zip(varsMTG.layerSpecifierVars, varsMTG.layerSpecifierTags)
  );

  static readonly status: Map<statusVarsUnion, statusNamesUnion> = new Map(
    zip(varsMTG.statusVars, varsMTG.statusNames)
  );
}

// ミストレにおける共通の計算式を定義
export const hpTobaseValue = (hp: number): number => {
  if (hp > 5000) {
    return Math.ceil(hp / 500) * 50;
  }
  return Math.ceil(hp / 200) * 20;
};
